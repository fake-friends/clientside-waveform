/*
<comp-waveform style="grid-area: wav;" number="${index}" class="play"
    file="url">
</comp-waveform>
*/

class CompWaveform extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: 'open'});
        sr.innerHTML = `
            <canvas id="player" onmouseover="this.getRootNode().host.hoverOn()" onmouseoff="this.getRootNode().host.hoverOff()" style="width: 100%; height: 100%"></canvas>
        `;

        let resizeTimeout = false;
        this.hoverState = false;
        const thisIndex = this.getAttribute("number");
        let width = sr.getElementById("player").offsetWidth;
        let height = sr.getElementById("player").offsetHeight;

        const canvas = sr.getElementById("player");
        canvas.width = width * dpr; //fix for retina displays
        canvas.height = height * dpr; //fix for retina displays
        canvas.style.width = width;
        canvas.style.height = height;
        const ctx = canvas.getContext("2d");
        const barCount = 100;

		let audioData;
		let barData;
		let playing = false;
        let duration;
		let startTime;
		let progress;

		const initDraw = [];
		for(let i = 0; i < barCount; i++){
		    initDraw.push(0.03);
		};
		drawWave(initDraw);

		let activeBuffer = audioContext.createBufferSource();
		activeBuffer.connect(analyser);

        function reduceBuffer(audioBuffer){
			audioData = audioBuffer;
			activeBuffer.buffer = audioBuffer;
            duration = audioBuffer.duration;
            const rawData = audioBuffer.getChannelData(0); // Only Use Left Channel
            const blockSize = Math.floor(rawData.length / barCount); //Number of samples per bar
            const filteredData = [];
            for(let i = 0; i < barCount; i++) {
                let blockStart = blockSize * i; // the location of the first sample in the block
                let sampleMax = 0;
                for(let j = 0; j < blockSize; j++){
                    if(sampleMax < Math.abs(rawData[blockStart + j])){
                        sampleMax = Math.abs(rawData[blockStart + j]);
                    };
                }
                if(sampleMax > 0.92){
                    sampleMax = 0.92;
                }
                if(sampleMax < 0.03){
                    sampleMax = 0.03;
                }
                filteredData.push(sampleMax);
            }
            barData = filteredData;
            return filteredData;
        };

        function drawWave(reduceBuffer){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.lineCap = "round";
            ctx.lineWidth = dpr * 2;
            let activeDraw;
            if(startTime != undefined){
                activeDraw = (audioContext.currentTime - startTime) / duration;
            }else{
                activeDraw = 0;
            };
            for (let h = 0; h < reduceBuffer.length; h++) {
                let xPos = (h + 0.5) * (canvas.width / barCount);
                let yStart = (canvas.height * 0.5) - (canvas.height * 0.5 * reduceBuffer[h]);
                let yEnd = (canvas.height * 0.5) + (canvas.height * 0.5 * reduceBuffer[h]);
                if(((h + 0.5) / barCount) < activeDraw){
                    ctx.strokeStyle = colorActive;
                }else{
                    ctx.strokeStyle = colorBright;
                };
                ctx.beginPath();
                ctx.moveTo(xPos, yStart);
                ctx.lineTo(xPos, yEnd);
                ctx.stroke();
            };
            if(playing == true){
                const grd = ctx.createLinearGradient((canvas.width * activeDraw) - (canvas.width * 0.1), 0, (canvas.width * activeDraw), 0);
                grd.addColorStop(0, "transparent");
                grd.addColorStop(1, colorActive.slice(0, -1) + " / 10%)");
                ctx.fillStyle = grd;
                ctx.fillRect((canvas.width * activeDraw), canvas.height * 0.015, (canvas.width * -0.1), canvas.height * 0.97);
                ctx.strokeStyle = colorActive;
                ctx.beginPath();
                ctx.moveTo(canvas.width * activeDraw, canvas.height * 0.025);
                ctx.lineTo(canvas.width * activeDraw, canvas.height * 0.975);
                ctx.stroke();
            };
        };

		function fetchAudio(url){
            fetch(url)
            .then(res => res.arrayBuffer())
            .then(buffer => audioContext.decodeAudioData(buffer))
            .then(decodedBuffer => reduceBuffer(decodedBuffer))
            .then(filteredBuffer => drawWave(filteredBuffer))
        };
        fetchAudio(this.getAttribute("file"));

        function animate(){
            if((thisIndex == activeIndex && playing == true) || (resizeTimeout == true)){
                if(resizeTimeout == true){
                    width = sr.getElementById("player").offsetWidth;
                    height = sr.getElementById("player").offsetHeight;
                    canvas.width = width * dpr; //fix for retina displays
                    canvas.height = height * dpr; //fix for retina displays
                    canvas.style.width = width;
                    canvas.style.height = height;
                }
                drawWave(barData);
                requestAnimationFrame(animate);
            };
        };

        let handlePlay = function(){
            if(playing == true){
                activeBuffer.stop();
                playing = false;
            }else if(thisIndex == activeIndex){
                activeBuffer = audioContext.createBufferSource();
                activeBuffer.connect(analyser);
                activeBuffer.buffer = audioData;
                activeBuffer.onended = () => {
                    playing = false;
                };
                activeBuffer.start();
                playing = true;
                startTime = audioContext.currentTime;
            };
            animate();
		}

        this.addEventListener("handlePlay", handlePlay);

		this.addEventListener("themeChange", e => {
            drawWave(barData);
        });

		function stopResize(){
		    resizeTimeout = false;
		};

        this.addEventListener("resize", e => {
            resizeTimeout = true;
            animate();
            setTimeout(stopResize(), 100);
        });
    };

    hoverOn(){
        this.hover = true;
    };

    hoverOff(){
        this.hover = false;
    };
};
window.customElements.define("comp-waveform", CompWaveform);
